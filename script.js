let t3 = document.getElementById('t3')
let holdingDisk = false
let diskAtHand
let diskToBeCompared

for (let i = 1; i <= 3; i++) {
  let tower = document.createElement('div')
  tower.setAttribute('id', 't' + i)
  tower.setAttribute('class', 'torre')
  document.getElementById('container').appendChild(tower)
}

let redDisk = document.createElement('div')
redDisk.setAttribute('id', 'redDisk')
redDisk.setAttribute('class', 'pilha')
document.getElementById('t1').appendChild(redDisk)

let greenDisk = document.createElement('div')
greenDisk.setAttribute('id', 'greenDisk')
greenDisk.setAttribute('class', 'pilha')
document.getElementById('t1').appendChild(greenDisk)

let purpleDisk = document.createElement('div')
purpleDisk.setAttribute('id', 'purpleDisk')
purpleDisk.setAttribute('class', 'pilha')
document.getElementById('t1').appendChild(purpleDisk)

let blueDisk = document.createElement('div')
blueDisk.setAttribute('id', 'blueDisk')
blueDisk.setAttribute('class', 'pilha')
document.getElementById('t1').appendChild(blueDisk)

document.getElementById('t1').addEventListener('click', moveDisk)
document.getElementById('t2').addEventListener('click', moveDisk)
document.getElementById('t3').addEventListener('click', moveDisk)

function moveDisk(evt) {

  let selected = evt.currentTarget
  if (holdingDisk === true) {
    releaseDisk(selected)
  } else {
    grabDisk(selected)
  }
}

function grabDisk(target) {

  diskAtHand = target.lastElementChild
  if (diskAtHand !== null) {
    holdingDisk = true
    diskAtHand.style.marginBottom = '8px'
  }
}

function releaseDisk(target) {

  diskToBeCompared = target.lastElementChild

  if (diskToBeCompared === null) {
    target.appendChild(diskAtHand)
    diskAtHand.style.marginBottom = '0px'
  } else if (diskToBeCompared.clientWidth > diskAtHand.clientWidth) {
    target.appendChild(diskAtHand)
  }

  diskAtHand.style.marginBottom = '0px'
  holdingDisk = false
  checkWinner()
}

function checkWinner (){

  if (document.getElementById('t3').childElementCount === 4) {

    document.body.innerHTML = ""

    let winner = document.createElement('div')
    winner.setAttribute('class', 'winner')
    document.body.appendChild(winner)

    let winningAnchor = document.createElement('a')
    winner.appendChild(winningAnchor)

    let winnerImage = document.createElement('img')
    winnerImage.setAttribute('src', 'https://media.giphy.com/media/iJc5ynT1VwAUPN55e6/giphy.gif')
    winnerImage.setAttribute('class', 'winnerImage')
    
    winningAnchor.setAttribute('href', 'index.html')
    winningAnchor.appendChild(winnerImage)
  }
}


